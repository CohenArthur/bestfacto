##IMPORT CELL##

import math

##CODING CELL##

def classic(n):

    if n == 0:
        return 1

    total = 1

    for i in range(1, n+1):

        total = total * i

    return total

def stirling(n):

    if n == 0:
        return 1

    return (n / math.e)**n * math.sqrt(2 * math.pi * n)


##EXECUTIVE CELL##

for i in range(10):

    print("Classic is ", classic(i))
    print("Stirling is ", stirling(i))